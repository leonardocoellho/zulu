package zulu.auth.domain

enum class Funcao(val value: Int) {
    OBSERVADOR(1),
    ADMINISTRADOR_LOCAL(9001)
}
