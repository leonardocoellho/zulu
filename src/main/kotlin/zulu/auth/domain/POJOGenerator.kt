package zulu.auth.domain

/**
 * Gerador de POJOS de Usuários Tabajara.
 * Use para seus testes (por sua conta e risco).
 **/

fun usuario1(): Usuario = Usuario(
        "US1",
        "matissek.bruno",
        "brasil",
        Funcao.ADMINISTRADOR_LOCAL
)

fun usuario2(): Usuario = Usuario(
        "US2",
        "almeida.junior",
        "selva",
        Funcao.OBSERVADOR
)