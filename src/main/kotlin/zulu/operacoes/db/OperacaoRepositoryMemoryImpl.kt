package zulu.operacoes.db

import org.springframework.stereotype.Repository
import zulu.operacoes.domain.*

@Repository
class OperacaoRepositoryMemoryImpl : OperacaoRepository {

    private object OperacaoSingleton {
        val operacaoMap : MutableMap<String, Operacao> = mutableMapOf(
                operacao1().id to operacao1(),
                operacao2().id to operacao2(),
                operacao3().id to operacao2()
        )
    }

    override fun getAll(): List<Operacao> = OperacaoSingleton.operacaoMap.values.toList()

    override fun getById(id : String): Operacao? = OperacaoSingleton.operacaoMap[id]

    override fun upsert(operacao: Operacao) {
        OperacaoSingleton.operacaoMap[operacao.id] = operacao
    }

}