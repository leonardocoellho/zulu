package zulu.graphql

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import zulu.operacoes.db.OperacaoRepositoryMemoryImpl
import zulu.operacoes.domain.Operacao

@Component
class OperacaoGraphQL : GraphQLQueryResolver, GraphQLMutationResolver {
    @Autowired
    val operacaoRepository = OperacaoRepositoryMemoryImpl()

    fun getOperacaoId(id: String) : Operacao? {
        return operacaoRepository.getById(id)
    }

    fun getAllOperacoes() : List<Operacao> {
        return operacaoRepository.getAll();
    }

    fun saveOperacao(operacao: Operacao): Boolean {
        operacaoRepository.upsert(operacao)
        return true
    }
}